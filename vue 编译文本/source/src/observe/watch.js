let id = 0;
class Watcher {
  // watch 有唯一标识
  constructor(vm, exprOrfn, cb = () => { }, opts = {}) {
    // exprOrfn 表达式  cb 回调
    this.vm = vm;
    this.exprOrfn = exprOrfn;
    if (typeof exprOrfn == 'function') {
      // getter 就是  Watcher 传入的第2个函数
      this.getter = exprOrfn;
    }
    this.cb = cb;
    this.opts = opts;
    this.id = id++;
    this.get()
  }
  get() {
  }
}
export default Watcher;
