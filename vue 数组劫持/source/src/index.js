import { initState } from './observe/index'
import Watcher from './observe/watch'
function Vue(options) {
  // console.log(options)
  // 初始化vue
  this._init(options)
}
Vue.prototype._init = function (options) {
  // vue 的初始化
  let vm = this;
  // 将 options 挂载到实例
  vm.$options = options;
  //需要数据重新初始化
  initState(vm)
  // 判断是否有el
  if (vm.$options.el) {
    vm.$mount();
  }
}

Vue.prototype.$mount = function () {
  let vm = this;
  let el = vm.$options.el
  el = vm.$el = query(el)  // 获取当前挂载的节点
  let updateComponent = () => {
    vm.$update()
    // console.log('数据更新')
  }
  // 渲染watchder
  new Watcher(vm, updateComponent)
}

function query(el) {
  if (typeof el === 'string') {
    return document.querySelector(el);
  }
  return el
}

Vue.prototype.$update = function () {
  let vm = this;
  let el = vm.$el
  // console.log(el)// 拿到节点
  // 需要匹配{{}}
  let node = document.createDocumentFragment();
  let firstChild;
  while (firstChild = el.firstChild) {
    node.appendChild(firstChild)
  }
  // console.log(node)  // 获取到文档碎片
  // todeo 对文本进行替换
  // compiler(node, vm)
  // el.appendChild(node)
}
export default Vue;