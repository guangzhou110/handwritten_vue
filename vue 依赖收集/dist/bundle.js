/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./source/src/index.js":
/*!*****************************!*\
  !*** ./source/src/index.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _observe_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observe/index */ "./source/src/observe/index.js");

function Vue(options) {
  // console.log(options)
  // 初始化vue
  this._init(options)
}
Vue.prototype._init = function (options) {
  // vue 的初始化
  let vm = this;
  // 将 options 挂载到实例
  vm.$options = options;
  //需要数据重新初始化
  Object(_observe_index__WEBPACK_IMPORTED_MODULE_0__["initState"])(vm)
  // 判断是否有el
  if (vm.$options.el) {
    vm.$mount();
  }
}

Vue.prototype.$mount = function () {
  let vm = this;
  let el = vm.$options.el
  el = vm.$el = query(el)  // 获取当前挂载的节点
  let updateComponent = () => {
    vm.$update()
    // console.log('数据更新')
  }
  // 渲染watchder
  new Watcher(vm, updateComponent)
}

function query(el) {
  if (typeof el === 'string') {
    return document.querySelector(el);
  }
  return el
}

Vue.prototype.$update = function () {
  let vm = this;
  let el = vm.$el
  // console.log(el)// 拿到节点
  // 需要匹配{{}}
  let node = document.createDocumentFragment();
  let firstChild;
  while (firstChild = el.firstChild) {
    node.appendChild(firstChild)
  }
  // console.log(node)  // 获取到文档碎片
  // todeo 对文本进行替换
  // compiler(node, vm)
  // el.appendChild(node)
}
/* harmony default export */ __webpack_exports__["default"] = (Vue);

/***/ }),

/***/ "./source/src/observe/array.js":
/*!*************************************!*\
  !*** ./source/src/observe/array.js ***!
  \*************************************/
/*! exports provided: arrayMethod, observerArray */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrayMethod", function() { return arrayMethod; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observerArray", function() { return observerArray; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./source/src/observe/index.js");
// // 改写7个方法
// // 拿到数组的所有方法
// let oldArrayProtoMethods = Array.prototype;
// // console.log(oldArrayProtoMethods)
// //Object.create()方法创建一个新对象,使用现有的对象来提供新创建的对象的__proto__
// export let arrayMethod = Object.create(oldArrayProtoMethods);
// console.log(arrayMethod)
// // 7个方法
// let methods = [
//   'push',
//   'pop',
//   'unshift',
//   'shift',
//   'sort',
//   'splice',
//   'reverse'
// ]
// // export function observerArray(inserted) {
// //   for (let i = 0; i < inserted.length; i++) {
// //     observe(inserted[i])
// //   }
// // }
// methods.forEach(method => {
//   console.log(method)
//   arrayMethod[method] = function (...args) {
//     console.log(...args)
//     let r = oldArrayProtoMethods[method].apply(this, args)
//     console.log('调取了数组更新方法')
//     console.log(r)
//   }
// })

// 改写7个方法
let oldArrayProtoMethods = Array.prototype;
// 拷贝的新的对象 可以查找到老的方法 
let arrayMethod = Object.create(oldArrayProtoMethods);
// console.log(arrayMethod)
let methods = [
  'push',
  'pop',
  'unshift',
  'shift',
  'sort',
  'splice',
  'reverse'
]
function observerArray(inserted) {
  for (let i = 0; i < inserted.length; i++) {
    Object(___WEBPACK_IMPORTED_MODULE_0__["observe"])(inserted[i])
    console.log('观测新增项')
  }
}
methods.forEach(method => {
  // console.log(method)
  arrayMethod[method] = function (...args) {
    let r = oldArrayProtoMethods[method].apply(this, args)
    let inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args
        break;
      case 'splice':
        // 获取新增的
        inserted = args.slice(2)
        break;
      default:
        break;
    }
    console.log(inserted, 'test')
    if (inserted) observerArray(inserted)
  }
})

/***/ }),

/***/ "./source/src/observe/index.js":
/*!*************************************!*\
  !*** ./source/src/observe/index.js ***!
  \*************************************/
/*! exports provided: initState, observe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initState", function() { return initState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observe", function() { return observe; });
/* harmony import */ var _observe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observe */ "./source/src/observe/observe.js");

function initState(vm) {
  // 拿到option  存储起来
  let options = vm.$options;
  if (options.data) {
    // 初始化数据
    initData(vm)
  }
  if (options.computed) {
    // 初始化计算属性
    initComputed()
  }
  if (options.watch) {
    // 初始化watch
    initWatch()
  }
}
function proxy(vm, source, key) {
  Object.defineProperty(vm, key, {
    get() {
      return vm[source][key]
    },
    set(newValue) {
      vm[source][key] = newValue
    }
  })
}
function initData(vm) {
  let data = vm.$options.data
  // 判断是否是函数 取返回值
  data = vm._data = typeof data === 'function' ? data.call(vm) : data || {}
  for (let key in data) {
    proxy(vm, "_data", key)
  }
  observe(vm._data)
}
function observe(data) {
  // 不是对象或者是null
  if (typeof data !== 'object' || data === null) {
    return
  }
  return new _observe__WEBPACK_IMPORTED_MODULE_0__["default"](data)
}
function initComputed() {

}
function initWatch() {

}

/***/ }),

/***/ "./source/src/observe/observe.js":
/*!***************************************!*\
  !*** ./source/src/observe/observe.js ***!
  \***************************************/
/*! exports provided: defineReactive, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defineReactive", function() { return defineReactive; });
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index */ "./source/src/observe/index.js");
/* harmony import */ var _array__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./array */ "./source/src/observe/array.js");


class Observe {
  constructor(data) {
    // 数组 重写push 方法
    if (Array.isArray(data)) {
      // 只能拦截数组方法 数组的每一个项还需要观测
      console.log('监听数组')
      data._proto = _array__WEBPACK_IMPORTED_MODULE_1__["arrayMethod"]
      // 观测数组的每一个项
      Object(_array__WEBPACK_IMPORTED_MODULE_1__["observerArray"])(data)
    } else {
      // 对象
      // data 就是我们 定义的 vm._data 的数据
      this.walk(data)
    }
  }
  //将对象的数据使用 defineProperty 重新定义 
  walk(data) {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];  // key
      let value = data[keys[i]];  // value
      defineReactive(data, key, value)
    }
  }
}
function defineReactive(data, key, value) {
  Object(_index__WEBPACK_IMPORTED_MODULE_0__["observe"])(value);
  Object.defineProperty(data, key, {
    get() {
      // 有值
      return value;
    },
    set(newValue) {
      if (newValue !== value) return;
      Object(_index__WEBPACK_IMPORTED_MODULE_0__["observe"])(newValue)
      value = newValue
    }
  })
}
/* harmony default export */ __webpack_exports__["default"] = (Observe);

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _source_src_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../source/src/index */ "./source/src/index.js");

let vm = new _source_src_index__WEBPACK_IMPORTED_MODULE_0__["default"]({
  el: '#app',
  data() {
    return {
      msg: 'hello',
      school: {
        name: 'zf',
        age: 10
      },
      arr: [{ a: 1 }, 2, 3]
    }
  },
  computed: {

  },
  watch: {

  }
})
console.log(vm.arr[0].a)  // hello




/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map