import { initState } from './observe/index'
import Watcher from './observe/watch'
// import { Compiler } from 'webpack'
function Vue(options) {
  // console.log(options)
  // 初始化vue
  this._init(options)
}
Vue.prototype._init = function (options) {
  // vue 的初始化
  let vm = this;
  // 将 options 挂载到实例
  vm.$options = options;
  //需要数据重新初始化
  initState(vm)

  if (vm.$options.el) {
    vm.$mount();
  }
}
function query(el) {
  if (typeof el === 'string') {
    return document.querySelector(el);
  }
  return el
}
Vue.prototype.$mount = function () {
  let vm = this;
  let el = vm.$options.el
  el = vm.$el = query(el)  // 获取当前挂载的节点
  let updateComponent = () => {
    vm.$update()
    // console.log('数据更新')
  }
  // 渲染watchder
  new Watcher(vm, updateComponent)
}
const defaultRe = /\{\{((?:.|\r?\n)+?)\}\}/

// 测试 正则
// "{{xxx}}".match(/\{\{((?:.|\r?\n)+?)\}\}/);
// (2)["{{xxx}}", "xxx", index: 0, input: "{{xxx}}", groups: undefined]
const util = {
  getValue(vm, expr) {
    // console.log(vm, expr)
    let keys = expr.split('.');
    return keys.reduce((arr, current) => {
      arr = arr[current]  // vm.school.name
      return arr
    }, vm)
  },
  compilerText(node, vm) {  //编译文本
    // 匹配正则
    node.textContent = node.textContent.replace(defaultRe, function (...args) {
      // console.log(args, 'test')
      return JSON.stringify(util.getValue(vm, args[1]))
    })
  }
}
function compiler(node, vm) {
  let childNodes = node.childNodes;
  ([...childNodes]).forEach(child => {
    // 1  元素  3  文本
    if (child.nodeType == 1) {
      // console.log('2020')
      compiler(child, vm)  //编译孩子节点
    } else if (child.nodeType == 3) {
      // console.log('111')
      util.compilerText(child, vm)
    }
  });
}
// 用户传入的数据去更新视图
Vue.prototype.$update = function () {
  let vm = this;
  let el = vm.$el
  console.log(el)// 拿到节点
  // 需要匹配{{}}
  let node = document.createDocumentFragment();
  let firstChild;
  while (firstChild = el.firstChild) {
    node.appendChild(firstChild)
  }
  // console.log(node)  // 获取到文档碎片
  // todeo 对文本进行替换
  compiler(node, vm)
  el.appendChild(node)
  console.log(el)
}
export default Vue;