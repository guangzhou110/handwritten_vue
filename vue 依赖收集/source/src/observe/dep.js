let id = 0;
// 发布 订阅
class Dep {
  constructor() {
    this.id = id++;
    this.subs = []
  }
  // 订阅 调用 addSub 将传入的参数保留到数组中
  addSub(watcher) {
    this.subs.push(watcher)
  }
  // 发布 遍历更新
  notify() {
    this.subs.forEach(watcher => {
      watcher.update()
    })
  }
  depend() {
    if (Dep.target) {
      Dep.target.addDep(this)
    }
  }
}
// 保存当前的watcher
// 将多个watch 存储起来拉 
let stack = [];
export function pushTarget(watcher) {
  Dep.target = watcher;
  stack.push(watcher)
}
// 取出当前的watch
// 将存储的watcher 取出来
export function popTarget(watcher) {
  // 取出
  stack.pop(watcher)
  // 将watcher 往前指
  Dep.target = stack[stack.length - 1]
}
// 收集依赖 收集的说一个个的watcher
export default Dep;