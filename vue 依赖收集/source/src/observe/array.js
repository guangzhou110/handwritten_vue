// // 改写7个方法
// // 拿到数组的所有方法
// let oldArrayProtoMethods = Array.prototype;
// // console.log(oldArrayProtoMethods)
// //Object.create()方法创建一个新对象,使用现有的对象来提供新创建的对象的__proto__
// export let arrayMethod = Object.create(oldArrayProtoMethods);
// console.log(arrayMethod)
// // 7个方法
// let methods = [
//   'push',
//   'pop',
//   'unshift',
//   'shift',
//   'sort',
//   'splice',
//   'reverse'
// ]
// // export function observerArray(inserted) {
// //   for (let i = 0; i < inserted.length; i++) {
// //     observe(inserted[i])
// //   }
// // }
// methods.forEach(method => {
//   console.log(method)
//   arrayMethod[method] = function (...args) {
//     console.log(...args)
//     let r = oldArrayProtoMethods[method].apply(this, args)
//     console.log('调取了数组更新方法')
//     console.log(r)
//   }
// })
import { observe } from ".";
// 改写7个方法
let oldArrayProtoMethods = Array.prototype;
// 拷贝的新的对象 可以查找到老的方法 
export let arrayMethod = Object.create(oldArrayProtoMethods);
// console.log(arrayMethod)
let methods = [
  'push',
  'pop',
  'unshift',
  'shift',
  'sort',
  'splice',
  'reverse'
]
export function observerArray(inserted) {
  for (let i = 0; i < inserted.length; i++) {
    observe(inserted[i])
    // console.log('观测新增项')
  }
}
methods.forEach(method => {
  // console.log(method)
  arrayMethod[method] = function (...args) {
    let r = oldArrayProtoMethods[method].apply(this, args)
    let inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args
        break;
      case 'splice':
        // 获取新增的
        inserted = args.slice(2)
        break;
      default:
        break;
    }
    // console.log(inserted, 'test')
    if (inserted) observerArray(inserted)
  }
})