import { observe } from './index'
import { arrayMethod, observerArray } from './array'
import Dep from './dep'
class Observe {
  constructor(data) {
    // 数组 重写push 方法
    if (Array.isArray(data)) {
      // 只能拦截数组方法 数组的每一个项还需要观测
      console.log('监听数组')
      data._proto = arrayMethod
      // 观测数组的每一个项
      observerArray(data)
    } else {
      // 对象
      // data 就是我们 定义的 vm._data 的数据
      this.walk(data)
    }
  }
  //将对象的数据使用 defineProperty 重新定义 
  walk(data) {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];  // key
      let value = data[keys[i]];  // value
      defineReactive(data, key, value)
    }
  }
}
export function defineReactive(data, key, value) {
  observe(value);
  let dep = new Dep()
  Object.defineProperty(data, key, {
    get() {
      // 如果有值 用的是渲染watcher
      if (Dep.target) {
        debugger;
        dep.addSub(Dep.target)
      }
      // 有值
      return value;
    },
    set(newValue) {
      if (newValue !== value) return;
      observe(newValue)
      value = newValue
    }
  })
}
export default Observe;