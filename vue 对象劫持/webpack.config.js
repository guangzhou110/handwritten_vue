let path = require('path')
let htmlwebpackplugin = require('html-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'source-map',  //产生源码映射文件
  plugins: [
    new htmlwebpackplugin({
      template: path.resolve(__dirname, './public/index.html')
    })
  ],
  // 更改解析模块的解析方式
  resolve: {
    modules: [path.resolve(__dirname, 'source'), path.resolve('node_modules')]
  }
}