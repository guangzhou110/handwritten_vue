import { initState } from './observe/index'
function Vue(options) {
  // console.log(options)
  // 初始化vue
  this._init(options)
}
Vue.prototype._init = function (options) {
  // vue 的初始化
  let vm = this;
  // 将 options 挂载到实例
  vm.$options = options;
  //需要数据重新初始化
  initState(vm)
}
export default Vue;