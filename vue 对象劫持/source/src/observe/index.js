import Observe from './observe'
export function initState(vm) {
  // 拿到option  存储起来
  let options = vm.$options;
  if (options.data) {
    // 初始化数据
    initData(vm)
  }
  if (options.computed) {
    // 初始化计算属性
    initComputed()
  }
  if (options.watch) {
    // 初始化watch
    initWatch()
  }
}
function proxy(vm, source, key) {
  Object.defineProperty(vm, key, {
    get() {
      return vm[source][key]
    },
    set(newValue) {
      vm[source][key] = newValue
    }
  })
}
function initData(vm) {
  let data = vm.$options.data
  // 判断是否是函数 取返回值
  data = vm._data = typeof data === 'function' ? data.call(vm) : data || {}
  for (let key in data) {
    proxy(vm, "_data", key)
  }
  observe(vm._data)
}
export function observe(data) {
  // 不是对象或者是null
  if (typeof data !== 'object' || data === null) {
    return
  }
  return new Observe(data)
}
function initComputed() {

}
function initWatch() {

}