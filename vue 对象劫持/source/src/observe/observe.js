import { observe } from './index'
class Observe {
  constructor(data) {
    // 数组 重写push 方法
    if (Array.isArray()) {
    } else {
      // 对象
      // data 就是我们 定义的 vm._data 的数据
      this.walk(data)
    }
  }
  //将对象的数据使用 defineProperty 重新定义 
  walk(data) {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];  // key
      let value = data[keys[i]];  // value
      defineReactive(data, key, value)
    }
  }
}
export function defineReactive(data, key, value) {
  observe(value);
  Object.defineProperty(data, key, {
    get() {
      // 有值
      return value;
    },
    set(newValue) {
      if (newValue !== value) return;
      value = newValue
    }
  })
}
export default Observe;